SELECT nomeplat, ano_lancamento
FROM plataforma AS p
WHERE EXISTS(
	SELECT *
    FROM fabricante AS f
    WHERE p.idfabricante = f.idfabricante
    AND f.ano_fundacao > 1970
);