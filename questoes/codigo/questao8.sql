SELECT ano_lancamento_jogo, nomeplat, nomejogo
FROM jogo NATURAL JOIN (
	SELECT idjogo, idplataforma, ano_lancamento_jogo
	FROM compativel_jogo_plataforma
	ORDER BY ano_lancamento_jogo ASC
	LIMIT 1
) AS ano NATURAL JOIN plataforma;