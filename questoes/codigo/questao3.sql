SELECT nomemid, velocidade_leitura
FROM midia AS m
WHERE m.velocidade_leitura BETWEEN 10 AND 30
ORDER BY m.velocidade_leitura DESC,
m.nomemid ASC;