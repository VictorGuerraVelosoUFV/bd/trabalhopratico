INSERT INTO `trabalhobd`.`fabricante` (`idfabricante`, `nomefab`, `ano_fundacao`) VALUES (1, 'Nintendo', 1889);
INSERT INTO `trabalhobd`.`fabricante` (`idfabricante`, `nomefab`, `ano_fundacao`) VALUES (2, 'Sony', 1946);
INSERT INTO `trabalhobd`.`fabricante` (`idfabricante`, `nomefab`, `ano_fundacao`) VALUES (3, 'Microsoft', 1975);
INSERT INTO `trabalhobd`.`fabricante` (`idfabricante`, `nomefab`, `ano_fundacao`) VALUES (4, 'Atari', 1972);


INSERT INTO `trabalhobd`.`plataforma` (`idplataforma`, `nomeplat`, `ano_lancamento`, `memoria`, `idfabricante`) VALUES (1, 'Atari 2600', 1977, '128', 4);
INSERT INTO `trabalhobd`.`plataforma` (`idplataforma`, `nomeplat`, `ano_lancamento`, `memoria`, `idfabricante`) VALUES (2, 'NES', 1985, '2048', 1);
INSERT INTO `trabalhobd`.`plataforma` (`idplataforma`, `nomeplat`, `ano_lancamento`, `memoria`, `idfabricante`) VALUES (3, 'SNES', 1990, '131072', 1);
INSERT INTO `trabalhobd`.`plataforma` (`idplataforma`, `nomeplat`, `ano_lancamento`, `memoria`, `idfabricante`) VALUES (4, 'Wii', 2006, '92274688', 1);
INSERT INTO `trabalhobd`.`plataforma` (`idplataforma`, `nomeplat`, `ano_lancamento`, `memoria`, `idfabricante`) VALUES (5, 'Xbox360', 2005, '536870912', 3);
INSERT INTO `trabalhobd`.`plataforma` (`idplataforma`, `nomeplat`, `ano_lancamento`, `memoria`, `idfabricante`) VALUES (6, 'PlayStation3', 2006, '536870912', 2);

INSERT INTO `trabalhobd`.`rede` (`idrede`, `nome`, `mensalidade`, `loja`, `idplataforma`) VALUES (1, 'Xbox Live', 19.99, true, 5);
INSERT INTO `trabalhobd`.`rede` (`idrede`, `nome`, `mensalidade`, `loja`, `idplataforma`) VALUES (2, 'PSN', 9.99, true, 6);
INSERT INTO `trabalhobd`.`rede` (`idrede`, `nome`, `mensalidade`, `loja`, `idplataforma`) VALUES (3, 'Nintendo Network', 0.0, true, 4);

INSERT INTO `trabalhobd`.`midia` (`idmidia`, `nomemid`, `velocidade_leitura`) VALUES (1, 'CD', 10);
INSERT INTO `trabalhobd`.`midia` (`idmidia`, `nomemid`, `velocidade_leitura`) VALUES (2, 'DVD', 20);
INSERT INTO `trabalhobd`.`midia` (`idmidia`, `nomemid`, `velocidade_leitura`) VALUES (3, 'Blu-ray', 40);
INSERT INTO `trabalhobd`.`midia` (`idmidia`, `nomemid`, `velocidade_leitura`) VALUES (4, 'Disquete', 1);
INSERT INTO `trabalhobd`.`midia` (`idmidia`, `nomemid`, `velocidade_leitura`) VALUES (5, 'SD', 100);
INSERT INTO `trabalhobd`.`midia` (`idmidia`, `nomemid`, `velocidade_leitura`) VALUES (6, 'Cartucho', 1000);
INSERT INTO `trabalhobd`.`midia` (`idmidia`, `nomemid`, `velocidade_leitura`) VALUES (7, 'HD-USB', 100);


INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (1, 'River Raid', 'Tiro', false, true);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (2, 'Mario Bros', 'Aventura', false, false);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (3, 'F-Zero', 'Corrida', false, false);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (4, 'Super Mario Bros', 'Aventura', false, true);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (5, 'Wii Sports', 'Esportes variados', true, true);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (6, 'FIFA12', 'Futebol', true, true);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (7, 'God of War 3', 'Ação', false, false);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (8, 'Forza MotorSport', 'Corrida realista', true, true);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (9, 'Gran Turismo 5', 'Corrida realista', true, true);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (10, 'Street Fighter V', 'Luta', false, true);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (11, 'The Witcher 3: Wild Hunt', 'Ação', false, false);
INSERT INTO `trabalhobd`.`jogo` (`idjogo`, `nomejogo`, `descricao`, `multiplayer`, `coop`) VALUES (12, 'Battlefield 4', 'Tiro', true, true);

INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (1, 'Season 1 character pack', 'Character Pack', 10);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (2, 'Season 2 character pack', 'Character Pack', 10);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (3, 'Season 3 character pack', 'Character Pack', 10);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (4, 'Halloween costumes', 'Costume Pack', 10);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (5, 'Xmas costumes', 'Costume Pack', 10);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (6, 'Blood and Wine', 'Expansion', 11);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (7, 'Heart of Stone', 'Expansion', 11);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (8, 'China Rising', 'Multiplayer Maps', 12);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (9, 'Second Assault', 'Multiplayer Maps', 12);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (10, 'Naval Strike', 'Multiplayer Maps', 12);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (11, 'Dragons Teeth', 'Multiplayer Maps', 12);
INSERT INTO `trabalhobd`.`dlc` (`iddlc`, `nomedlc`, `descricao`, `idjogo`) VALUES (12, 'Final Stand', 'Multiplayer Maps', 12);

INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (1, 6);
INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (2, 4);
INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (2, 6);
INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (3, 6);
INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (4, 2);
INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (4, 5);
INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (5, 2);
INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (5, 7);
INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (6, 3);
INSERT INTO `trabalhobd`.`compativel_midia` (`idplataforma`, `idmidia`) VALUES (6, 7);



INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (1, 1, 2, 1982);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (1, 2, 2, 1983);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (2, 2, 1, 1983);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (3, 3, 1, 1990);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (4, 3, 1, 2007);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (2, 4, 1, 1985);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (4, 4, 1, 2007);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (4, 5, 1, 2006);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (4, 6, 1, 2011);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (5, 6, 1, 2011);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (6, 6, 1, 2011);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (6, 7, 2, 2012);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (5, 8, 1, 2011);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (6, 9, 1, 2010);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (5, 10, 2, 2008);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (6, 10, 1, 2008);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (5, 11, 1, 2015);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (5, 12, 1, 2013);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (6, 11, 1, 2015);
INSERT INTO `trabalhobd`.`compativel_jogo_plataforma` (`idplataforma`, `idjogo`, `quantidade`, `ano_lancamento_jogo`) VALUES (6, 12, 1, 2013);













