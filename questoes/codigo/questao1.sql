SELECT DISTINCT nomeplat
FROM plataforma AS p
WHERE EXISTS(
	SELECT *
    FROM compativel_midia AS c
    WHERE p.idplataforma = c.idplataforma
    AND EXISTS(
		SELECT *
        FROM midia AS m
        WHERE c.idmidia = m.idmidia
        AND m.velocidade_leitura > 100
    )
);