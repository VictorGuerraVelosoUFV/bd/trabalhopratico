SELECT SUM(jogos_lancados*memoria)/SUM(jogos_lancados)
FROM plataforma NATURAL JOIN (
	SELECT idplataforma, COUNT(*) AS jogos_lancados
	FROM compativel_jogo_plataforma
	GROUP BY idplataforma
) AS jogos;