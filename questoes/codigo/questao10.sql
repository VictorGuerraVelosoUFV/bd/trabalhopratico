SELECT nomefab
FROM (
	SELECT nomefab, COUNT(idmidia) AS num_midias
	FROM (
		SELECT DISTINCT nomefab, idmidia
		FROM fabricante NATURAL JOIN plataforma NATURAL JOIN compativel_midia
	) AS fab_midias
	GROUP BY nomefab
) AS fab_num_midias
WHERE num_midias > 1;