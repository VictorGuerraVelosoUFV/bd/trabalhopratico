SELECT nomeplat, nomefab, (
	SELECT COALESCE( -- replace nulls with 0s
		SUM(j.quantidade),0
	)
    FROM compativel_jogo_plataforma AS j
    WHERE j.idplataforma = p.idplataforma
) AS numero_jogos
FROM plataforma AS p NATURAL JOIN fabricante
ORDER BY nomeplat;