SELECT nomejogo
FROM jogo NATURAL JOIN (
	SELECT c.idjogo, COUNT(c.idplataforma) AS num_plat
	FROM compativel_jogo_plataforma AS c
	GROUP BY c.idjogo
) AS p
WHERE num_plat > 1;