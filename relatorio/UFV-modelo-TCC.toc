\changetocdepth {4}
\babel@toc {brazil}{}
\contentsline {chapter}{\chapternumberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{6}{chapter.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{6}{section.1.1}
\contentsline {section}{\numberline {1.2}Infraestrutura}{6}{section.1.2}
\contentsline {section}{\numberline {1.3}Configura\IeC {\c c}\IeC {\~a}o inicial}{6}{section.1.3}
\contentsline {chapter}{\chapternumberline {2}Desenvolvimento}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Quest\IeC {\~a}o 1}{11}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}C\IeC {\'o}digo}{11}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Resultado}{11}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Quest\IeC {\~a}o 2}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}C\IeC {\'o}digo}{11}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Resultado}{12}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Quest\IeC {\~a}o 3}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}C\IeC {\'o}digo}{12}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Resultado}{12}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Quest\IeC {\~a}o 4}{13}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}C\IeC {\'o}digo}{13}{subsection.2.4.1}
\contentsline {section}{\numberline {2.5}Quest\IeC {\~a}o 5}{13}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}C\IeC {\'o}digo}{13}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Resultado}{13}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Quest\IeC {\~a}o 6}{14}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}C\IeC {\'o}digo}{14}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Resultado}{14}{subsection.2.6.2}
\contentsline {section}{\numberline {2.7}Quest\IeC {\~a}o 7}{14}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}C\IeC {\'o}digo}{14}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Resultado}{15}{subsection.2.7.2}
\contentsline {section}{\numberline {2.8}Quest\IeC {\~a}o 8}{15}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}C\IeC {\'o}digo}{15}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}Resultado}{15}{subsection.2.8.2}
\contentsline {section}{\numberline {2.9}Quest\IeC {\~a}o 9}{16}{section.2.9}
\contentsline {subsection}{\numberline {2.9.1}C\IeC {\'o}digo}{16}{subsection.2.9.1}
\contentsline {subsection}{\numberline {2.9.2}Resultado}{16}{subsection.2.9.2}
\contentsline {section}{\numberline {2.10}Quest\IeC {\~a}o 10}{16}{section.2.10}
\contentsline {subsection}{\numberline {2.10.1}C\IeC {\'o}digo}{16}{subsection.2.10.1}
\contentsline {subsection}{\numberline {2.10.2}Resultado}{17}{subsection.2.10.2}
\contentsline {section}{\numberline {2.11}Quest\IeC {\~a}o 11}{17}{section.2.11}
\contentsline {subsection}{\numberline {2.11.1}Modifica\IeC {\c c}\IeC {\~o}es propostas no diagrama}{17}{subsection.2.11.1}
\contentsline {subsubsection}{\numberline {2.11.1.1}Novas tabelas}{17}{subsubsection.2.11.1.1}
\contentsline {subsubsection}{\numberline {2.11.1.2}Novos atributos}{18}{subsubsection.2.11.1.2}
\contentsline {subsection}{\numberline {2.11.2}C\IeC {\'o}digo SQL gerado}{18}{subsection.2.11.2}
\contentsline {section}{\numberline {2.12}Quest\IeC {\~a}o 12}{21}{section.2.12}
\contentsline {section}{\numberline {2.13}Quest\IeC {\~a}o 13a}{26}{section.2.13}
\contentsline {subsection}{\numberline {2.13.1}Exerc\IeC {\'\i }cio proposto}{26}{subsection.2.13.1}
\contentsline {subsection}{\numberline {2.13.2}C\IeC {\'o}digo}{26}{subsection.2.13.2}
\contentsline {subsection}{\numberline {2.13.3}Resultado}{27}{subsection.2.13.3}
\contentsline {section}{\numberline {2.14}Quest\IeC {\~a}o 13b}{27}{section.2.14}
\contentsline {subsection}{\numberline {2.14.1}Exerc\IeC {\'\i }cio proposto}{27}{subsection.2.14.1}
\contentsline {subsection}{\numberline {2.14.2}C\IeC {\'o}digo}{27}{subsection.2.14.2}
\contentsline {subsection}{\numberline {2.14.3}Resultado}{28}{subsection.2.14.3}
\vspace {\cftbeforepartskip }
