SELECT nomejogo
FROM (
	SELECT idjogo, nomejogo, MIN(mensalidade) AS mensalidade
	FROM jogo 
    NATURAL JOIN compativel_jogo_plataforma
	NATURAL JOIN (
		SELECT idplataforma, mensalidade
		FROM rede NATURAL JOIN plataforma
	) AS rede_mensalidade
	WHERE multiplayer=1
	GROUP BY idjogo
) AS jogos_desordenados
ORDER BY mensalidade, nomejogo;