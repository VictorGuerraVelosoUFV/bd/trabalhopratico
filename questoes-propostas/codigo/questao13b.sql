CREATE TABLE preco_plat (
  `idplataforma` INT NOT NULL,
  `preco` INT NOT NULL,
  PRIMARY KEY (`idplataforma`)
);

INSERT INTO preco_plat VALUES (1, 80);
INSERT INTO preco_plat VALUES (2, 80);
INSERT INTO preco_plat VALUES (3, 80);
INSERT INTO preco_plat VALUES (4, 100);
INSERT INTO preco_plat VALUES (5, 100);
INSERT INTO preco_plat VALUES (6, 150);

SELECT idplataforma, SUM(preco) AS total
FROM (
	SELECT idplataforma, COUNT(iddlc)*preco_plat.preco*0.5 AS preco
	FROM dlc 
	NATURAL JOIN (
		SELECT idplataforma, idjogo
		FROM compativel_jogo_plataforma
	) AS jogo_plataforma
	NATURAL JOIN preco_plat
	GROUP BY idplataforma
	UNION ALL
	SELECT idplataforma, COUNT(idjogo)*preco_plat.preco AS preco
	FROM compativel_jogo_plataforma NATURAL JOIN preco_plat
	GROUP BY idplataforma
) AS T
GROUP BY idplataforma;
DROP TABLE preco_plat;